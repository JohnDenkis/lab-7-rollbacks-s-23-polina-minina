# Part 1 (2/3 points)

Create tables, insert values:

```
CREATE DATABASE sqr;
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO Player (username, balance) VALUES ('Bob', 200);
INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
```

# Part 2 (3/3 points)

Create new table Inventory which stores username, product name and amount of this product in inventory:

```
CREATE TABLE Inventory (
    username TEXT,
    product TEXT, 
    amount INT CHECK(amount >= 0),
    UNIQUE(username, product)
);
```

Count total size of inventory of the user:
```
SELECT SUM(amount) FROM Inventory WHERE username = %(username)s
```

Update or insert inventory for user:

```
INSERT INTO Inventory (username, product, amount) 
VALUES (%(username)s, %(product)s, %(amount)s)
ON CONFLICT (username, product) 
DO UPDATE SET amount = Inventory.amount + %(amount)s"
```

Constraint that in inventory player can store only 100 products in total. 
We create a trigger function:

```
CREATE OR REPLACE FUNCTION invertory_limit() 
RETURNS TRIGGER AS $$
DECLARE
    total INT;
BEGIN
    SELECT SUM(amount) INTO total FROM Inventory 
    WHERE username = NEW.username;
    IF total + NEW.amount > 100 THEN
        RAISE EXCEPTION 'Unable to add products because there is no space in the inventory :(';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
```

Then, next lines call trigger before each indert or update on Inventory:

```
CREATE TRIGGER inventory_limit_trigger
  BEFORE INSERT OR UPDATE ON Inventory 
  FOR EACH ROW
  EXECUTE FUNCTION invertory_limit();
```


# Extra part 1 (1.5 points)

While the user tries to resend the request, duplicates may occur,
so extra products will be added to the inventory. 
To avoid this situation, we can attach a unique ID to each transaction. 
Also, we store all transaction IDs and results in a separate table. 
Thus, the client can check if the transaction completed successfully 
and make a decision to resend request.