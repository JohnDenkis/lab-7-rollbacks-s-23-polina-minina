import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"

inventory_total = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"
products_insert = "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) " \
                   "ON CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + %(amount)s"
MAX_PRODUCTS = 100


def get_connection():
    return psycopg2.connect(
        dbname="sqr",
        user="postgres",
        password="postgres",
        host="localhost",
        port=5332
    )


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")

                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                # Count total
                cur.execute(inventory_total, obj)
                total_inventory = cur.fetchone()
                if total_inventory is None:
                    total_inventory = 0
                else:
                    total_inventory = 0 if total_inventory[0] is None else total_inventory[0]

                # Check condition on MAX_PRODUCTS
                if total_inventory + amount > MAX_PRODUCTS:
                    raise Exception("Unable to add products because there is no space in the inventory :(")

                # Insert/update new data
                cur.execute(products_insert, obj)
                if cur.rowcount != 1:
                    raise Exception("Error updating inventory")
            except Exception as e:
                conn.rollback()
                raise e
